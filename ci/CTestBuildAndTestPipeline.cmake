# Get platform specific build info
include($ENV{PLATFORM_FILE})

# Run CTest
ctest_start(${CTEST_BUILD_MODEL})
ctest_configure(BUILD ${CTEST_BINARY_DIRECTORY} SOURCE ${CTEST_SOURCE_DIRECTORY}
                OPTIONS "${OPTIONS}")
ctest_build()
set(CTEST_TEST_TIMEOUT 30)
ctest_test(BUILD ${CTEST_BINARY_DIRECTORY})
ctest_submit()
