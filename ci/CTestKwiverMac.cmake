set(CTEST_SITE $ENV{HOST})
set(CTEST_SOURCE_DIRECTORY $ENV{PROJECT_SRC_DIR})
set(CTEST_BINARY_DIRECTORY $ENV{PROJECT_BUILD_DIR})
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND $ENV{BUILD_COMMAND})
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_PROJECT_NAME Kwiver)
set(CTEST_BUILD_MODEL "Experimental")
set(CTEST_USE_LAUNCHERS 1)
include(CTestUseLaunchers)
set(OPTIONS
          "-DKWIVER_ENABLE_ARROWS=ON"
          "-DKWIVER_ENABLE_CERES=ON"
          "-DKWIVER_ENABLE_C_BINDINGS=ON"
          "-DKWIVER_ENABLE_DOCS=OFF"
          "-DKWIVER_ENABLE_FFMPEG=ON"
          "-DKWIVER_ENABLE_GDAL:BOOL=ON"
          "-DKWIVER_ENABLE_KPF=ON"
          "-DKWIVER_ENABLE_LOG4CXX=OFF"
          "-DKWIVER_ENABLE_LOG4CPLUS=ON"
          "-DKWIVER_ENABLE_MVG:BOOL=ON"
          "-DKWIVER_ENABLE_OPENCV=ON"
          "-DKWIVER_ENABLE_PROCESSES=ON"
          "-DKWIVER_ENABLE_PROJ=ON"
          "-DKWIVER_ENABLE_PYTHON=ON"
          "-DKWIVER_ENABLE_SERIALIZE_JSON=ON"
          "-DKWIVER_ENABLE_SERIALIZE_PROTOBUF=ON"
          "-DKWIVER_ENABLE_SPROKIT=ON"
          "-DKWIVER_ENABLE_TESTS=ON"
          "-DKWIVER_ENABLE_TOOLS=ON"
          "-DKWIVER_ENABLE_TRACK_ORACLE=ON"
          "-DKWIVER_ENABLE_VISCL=OFF"
          "-DKWIVER_ENABLE_VXL=ON"
          "-DKWIVER_USE_BUILD_TREE=ON")

set(platform Mac)
set(ENV{CC} gcc)
set(ENV{CXX} g++)
set(compiler "GCC-4.9.2")
